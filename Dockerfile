FROM java:8

EXPOSE 8080

ADD target/insside-biller.jar insside-biller.jar

ENTRYPOINT ["java", "-jar", "insside-biller.jar"]