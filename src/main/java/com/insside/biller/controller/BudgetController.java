package com.insside.biller.controller;

import com.google.common.collect.ImmutableList;
import com.insside.biller.pojo.model.BudgetTO;
import com.insside.biller.pojo.model.response.BudgetResponseTO;
import com.insside.biller.service.BudgetService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(path = "/api/v1/budgets", produces = MediaType.APPLICATION_JSON_VALUE)
public class BudgetController {

    @Autowired
    private BudgetService service;

    @PostMapping
    public void saveBudget(@Valid final @RequestBody BudgetTO budgetTO) {
        log.info(String.format("Saving budget code:%s", budgetTO.getCode()));
        service.save(budgetTO);
        log.info("Budget saved.");

    }

    @PutMapping("/{code}")
    public void updateBudget(@Valid final @PathVariable("code") String code, final @RequestBody BudgetTO budgetTO) {
        log.info(String.format("Updating budget code:%s", budgetTO.getCode()));
        service.update(budgetTO);
        log.info("Budget updated.");

    }

    @GetMapping("/filtering")
    public BudgetResponseTO getServiceByCode(@Valid final @RequestParam("code") String code) {
        log.info(String.format("get budget with code:%s ", code));
        return service.getBudgetByCode(code);
    }

    @GetMapping
    public ImmutableList<BudgetTO> getAllBudgets() {
        log.info("Finding all budgets");
        return service.findAllBudgets();
    }

    @DeleteMapping("/{id}")
    public void deleteBudgetFisicalyByCode(@Valid final @PathVariable("id") Long id){
        log.info(String.format("Deleting budget id:%s ", id));
        service.delete(id);
        log.info("Budget deleted fisicaly.");
    }
}
