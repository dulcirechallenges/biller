package com.insside.biller.controller;

import com.insside.biller.exception.InssideBudgetException;
import com.insside.biller.exception.InssideServiceException;
import com.insside.biller.exception.NotFoundException;
import com.insside.biller.pojo.model.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.service.spi.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.insside.biller.pojo.enums.ErrorStatus.GENERIC;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.ResponseEntity.status;

@Slf4j
@RestControllerAdvice
public class AdviseController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ServiceException.class)
    public ResponseEntity<ApiResponse> handledServiceException(final ServiceException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ApiResponse.builder().code("01").message(e.getMessage()).build());
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ApiResponse> handledNotFoundException(final NotFoundException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ApiResponse.builder().code("02").message(e.getMessage()).build());
    }

    @ExceptionHandler(InssideServiceException.class)
    public ResponseEntity<ApiResponse> handledInssideServiceException(final InssideServiceException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ApiResponse.builder().code("03").message(e.getMessage()).build());
    }

    @ExceptionHandler(InssideBudgetException.class)
    public ResponseEntity<ApiResponse> handledInssideBudgetException(final InssideBudgetException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ApiResponse.builder().code("04").message(e.getMessage()).build());
    }

    /**
     * 500
     * Handle unknown exceptions
     */
    @ExceptionHandler({Exception.class})
    public ResponseEntity<ApiResponse> unhandledException(final Exception e) {
        log.error("Unknown exception: ", e);
        return status(INTERNAL_SERVER_ERROR).body(new ApiResponse("00", GENERIC.getDescription()));
    }
}
