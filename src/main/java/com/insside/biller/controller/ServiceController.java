package com.insside.biller.controller;

import com.google.common.collect.ImmutableList;
import com.insside.biller.pojo.model.ServiceTO;
import com.insside.biller.service.ServiceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(path = "/api/v1/services", produces = MediaType.APPLICATION_JSON_VALUE)
public class ServiceController {

    @Autowired
    private ServiceService service;

    @PostMapping
    public void saveService(@Valid final @RequestBody ServiceTO serviceTO) {
        log.info(String.format("Saving service code:%s name:%s", serviceTO.getCode(), serviceTO.getName()));
        service.save(serviceTO);
        log.info("Service saved.");

    }

    @PutMapping("/{code}")
    public void updateService(@Valid final @PathVariable("code") String code, final @RequestBody ServiceTO serviceTO) {
        log.info(String.format("Updating service code:%s name:%s", serviceTO.getCode(), serviceTO.getName()));
        service.update(serviceTO);
        log.info("Service updated.");

    }

    @GetMapping("/filtering")
    public ServiceTO getServiceByCode(@Valid final @RequestParam("code") String code) {
        log.info(String.format("get service with code:%s ", code));
        return service.getServiceByCode(code);
    }

    @GetMapping
    public ImmutableList<ServiceTO> getAllServices() {
        log.info("Finding all services");
        return service.findAllServices();
    }

    @DeleteMapping("/{id}")
    public void deleteServiceFisicalyByCode(@Valid final @PathVariable("id") Long id) {
        log.info(String.format("Deleting service id:%s ", id));
        service.delete(id);
        log.info("Service deleted fisicaly.");
    }
}
