package com.insside.biller.pojo.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode
public class BaseEntityTO {

    private Long id;

    private LocalDateTime createdAt;

    private LocalDateTime updated;

    private Boolean status;
}
