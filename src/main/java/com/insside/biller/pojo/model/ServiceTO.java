package com.insside.biller.pojo.model;

import com.insside.biller.pojo.enums.Area;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ServiceTO extends BaseEntityTO {

    private String code;

    private String name;

    private String detail;

    private Area area;

    private BigDecimal amount;


}
