package com.insside.biller.pojo.model.request;

import com.insside.biller.pojo.enums.ServiceTypeCode;
import com.insside.biller.pojo.model.ServiceTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ServiceBudgetRequest {

    private ServiceTO serviceTO;

    private ServiceTypeCode serviceTypeCode;
}
