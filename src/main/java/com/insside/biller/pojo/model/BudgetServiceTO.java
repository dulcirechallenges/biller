package com.insside.biller.pojo.model;

import com.insside.biller.pojo.entity.Budget;
import com.insside.biller.pojo.entity.Service;
import com.insside.biller.pojo.enums.ServiceTypeCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BudgetServiceTO extends BaseEntityTO {

    private Service service;

    private Budget budget;

    private ServiceTypeCode serviceType;
}
