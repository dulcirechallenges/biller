package com.insside.biller.pojo.model;

import com.insside.biller.pojo.enums.PaymentType;
import com.insside.biller.pojo.model.request.ServiceBudgetRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BudgetTO extends BaseEntityTO {


    private String code;

    private LocalDateTime dueDate;

    private String company;

    private String client;

    private PaymentType paymentType;

    private Long paymentDays;

    private String discountJSON;

    private Map<String, BigDecimal> discount;

    private List<ServiceBudgetRequest> serviceTOList;

}
