package com.insside.biller.pojo.model.response;

import com.insside.biller.pojo.model.BaseEntityTO;
import com.insside.biller.pojo.model.BudgetServiceTO;
import com.insside.biller.pojo.model.BudgetTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BudgetResponseTO extends BaseEntityTO {


    private BudgetTO budgetTO;

    private List<BudgetServiceTO> serviceTOList;

}
