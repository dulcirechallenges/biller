package com.insside.biller.pojo.enums;

import lombok.Getter;

@Getter
public enum PaymentType {
    TRANSFER,
    DEBIT,
    CREDIT
}
