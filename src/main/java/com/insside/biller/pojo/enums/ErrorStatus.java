package com.insside.biller.pojo.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorStatus {

    /** COMMONS */
    INVALID_PARAMETER("There are one or more parameters that don’t satisfy one or more API restrictions"),
    RESOURCE_NOT_FOUND("The resource for which you are requesting was not found"),
    GENERIC("Untyped error"),
    EXPIRED_TOKEN("Session token expired"),
    INVALID_TOKEN("Session token invalid"),
    BAD_CREDENTIALS("The credentials are invalid"),
    UNAUTHORIZED_CLIENT("The client doesn't have permission for the resource"),
    DISABLED_RESOURCE("The resource is disabled"),
    PARAMS_ERROR("Some params are invalid");

    private String description;

}
