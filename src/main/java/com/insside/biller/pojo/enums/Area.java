package com.insside.biller.pojo.enums;

import lombok.Getter;

@Getter
public enum Area {
    AUDIT_CONSULTING,
    DEVELOP,
    CYBERSECURITY

}
