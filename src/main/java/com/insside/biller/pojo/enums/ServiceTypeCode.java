package com.insside.biller.pojo.enums;

import lombok.Getter;

@Getter
public enum ServiceTypeCode {

    OFFERED,
    OPTIONAL

}
