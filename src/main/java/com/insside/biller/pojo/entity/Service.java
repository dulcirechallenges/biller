package com.insside.biller.pojo.entity;

import com.insside.biller.pojo.enums.Area;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Table(name = "service")
public class Service extends BaseEntity {

    @NotBlank(message = "code is mandatory")
    @Column(name = "code", unique = true, nullable = false)
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "detail")
    private String detail;

    @Column(name = "area")
    @Enumerated(EnumType.STRING)
    private Area area;

    @Column(name = "amount")
    private BigDecimal amount;


}
