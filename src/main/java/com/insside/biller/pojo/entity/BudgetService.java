package com.insside.biller.pojo.entity;

import com.insside.biller.pojo.enums.ServiceTypeCode;
import lombok.*;

import javax.persistence.*;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.FetchType.EAGER;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper = true)
@Table(name = "budget_services",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"budget_id", "service_id"})}
)
public class BudgetService extends BaseEntity {

    @JoinColumn(name = "service_id", foreignKey = @ForeignKey(name = "fk_service_id_to_budget"))
    @ManyToOne(cascade = MERGE, fetch = EAGER)
    private Service service;

    @JoinColumn(name = "budget_id", foreignKey = @ForeignKey(name = "fk_budget_id_to_service"))
    @ManyToOne(cascade = MERGE, fetch = EAGER)
    private Budget budget;

    @Column(name = "service_type")
    @Enumerated(EnumType.STRING)
    private ServiceTypeCode serviceType;
}
