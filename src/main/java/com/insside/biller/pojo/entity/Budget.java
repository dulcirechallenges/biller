package com.insside.biller.pojo.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.insside.biller.pojo.entity.converter.HashMapConverter;
import com.insside.biller.pojo.enums.PaymentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "budget")
public class Budget extends BaseEntity {

    @NotBlank(message = "code is mandatory")
    @Column(name = "code", unique = true)
    private String code;

    @Column(name = "due_date")
    private LocalDateTime dueDate;

    @Column(name = "company")
    private String company;

    @Column(name = "client")
    private String client;

    @Column(name = "payment_type")
    @Enumerated(EnumType.STRING)
    private PaymentType paymentType;

    @Column(name = "payment_days")
    private Long paymentDays;

    @Column(name = "total")
    private BigDecimal total;

    @Transient
    private String discountJSON;

    @Convert(converter = HashMapConverter.class)
    private Map<String, Object> discount;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public void serializeCustomerAttributes() throws JsonProcessingException {
        this.discountJSON = objectMapper.writeValueAsString(discount);
    }

    public void deserializeCustomerAttributes() throws IOException {
        this.discount = objectMapper.readValue(discountJSON, Map.class);
    }

}
