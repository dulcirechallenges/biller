package com.insside.biller.exception;

public class InssideBudgetException extends RuntimeException {

    public InssideBudgetException(final String message) {
        super(message);
    }

}