package com.insside.biller.exception;

public class InssideServiceException extends RuntimeException {

    public InssideServiceException(final String message) {
        super(message);
    }

}