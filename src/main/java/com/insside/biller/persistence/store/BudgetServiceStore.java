package com.insside.biller.persistence.store;

import com.insside.biller.persistence.repository.BudgetServiceRepository;
import com.insside.biller.pojo.entity.BudgetService;
import com.insside.biller.pojo.model.BudgetServiceTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BudgetServiceStore extends Store<BudgetService, BudgetServiceTO> {

    @Autowired
    private BudgetServiceRepository repository;

    public List<BudgetServiceTO> findByBudgetId(final Long id) {
        return repository.findByBudgetId(id).stream().map(d -> mapper.map(d, BudgetServiceTO.class)).collect(Collectors.toList());
    }

    public void deleteByBudgetId(final Long id){
        repository.deleteByBudgetId(id);
    }

    public void deleteById(final Long id){
        repository.deleteById(id);
    }

    @Override
    protected JpaRepository<BudgetService, Long> getRepository() {
        return repository;
    }

    @Override
    protected Class<BudgetService> entityClass() {
        return BudgetService.class;
    }

    @Override
    protected Class<BudgetServiceTO> dtoClass() {
        return BudgetServiceTO.class;
    }
}
