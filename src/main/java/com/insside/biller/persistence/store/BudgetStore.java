package com.insside.biller.persistence.store;

import com.insside.biller.persistence.repository.BudgetRepository;
import com.insside.biller.pojo.entity.Budget;
import com.insside.biller.pojo.entity.converter.HashMapConverter;
import com.insside.biller.pojo.model.BudgetTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Component
public class BudgetStore extends Store<Budget, BudgetTO> {

    @Autowired
    private BudgetRepository repository;

    public Optional<BudgetTO> findByCode(final String code) {
        return repository.findByCode(code).map(d -> mapper.map(d, BudgetTO.class));
    }

    public void deleteById(final Long id) {
        repository.deleteById(id);
    }

    @Override
    protected JpaRepository<Budget, Long> getRepository() {
        return repository;
    }

    @Override
    protected Class<Budget> entityClass() {
        return Budget.class;
    }

    @Override
    protected Class<BudgetTO> dtoClass() {
        return BudgetTO.class;
    }
}
