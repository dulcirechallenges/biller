package com.insside.biller.persistence.store;

import com.google.common.collect.ImmutableList;
import com.insside.biller.exception.NotFoundException;
import com.insside.biller.pojo.entity.BaseEntity;
import com.insside.biller.pojo.model.BaseEntityTO;
import com.insside.biller.util.Mapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;
import static com.insside.biller.util.CollectionUtils.safeStream;
import static java.lang.String.format;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;


/**
 * Layer used to abstract the Spring Data framework to the domain.
 */
@SuppressWarnings("PMD.TooManyStaticImports")
public abstract class Store<E extends BaseEntity, D extends BaseEntityTO> {

    @Autowired
    protected Mapper mapper;

    protected abstract JpaRepository<E, Long> getRepository();

    protected abstract Class<E> entityClass();

    protected abstract Class<D> dtoClass();


    /**
     * Avoid to query by a null value.
     */
    private static Predicate safe(final Object valueToQuery, final Predicate predicate) {
        return shouldIgnore(valueToQuery) ? null : predicate;
    }


    /**
     * Return if the value to filter should be ignored in query. It should be ignored when is null or blank.
     */
    private static boolean shouldIgnore(final Object filterValue) {
        return isNull(filterValue) || (filterValue instanceof String) && StringUtils.isEmpty((String) filterValue);
    }


    /**
     * Equals specification.
     */
    protected Specification<E> equals(final String field, final Object value) {
        return (root, cq, cb) -> safe(value, cb.equal(root.get(field), value));
    }


    /**
     * Greater or equals specification for LocalDateTIme.
     */
    protected Specification<E> greaterThanOrEqualTo(final String field, final LocalDateTime value) {
        return (root, cq, cb) -> safe(value, cb.greaterThanOrEqualTo(root.get(field), value));
    }

    /**
     * Greater or equals specification for LocalDateTIme.
     */
    protected Specification<E> greaterThan(final String field, final Integer value) {
        return (root, cq, cb) -> safe(value, cb.greaterThan(root.get(field), value));
    }

    /**
     * Less specification for LocalDateTIme.
     */
    protected Specification<E> lessThan(final String field, final LocalDateTime value) {
        return (root, cq, cb) -> safe(value, cb.lessThanOrEqualTo(root.get(field), value));
    }


    /**
     * In specification.
     */
    protected <T> Specification<E> in(final String field, final Collection<T> value) {
        return (root, cq, cb) -> {
            final Collection<T> safeCollection = CollectionUtils.emptyIfNull(value);
            return safe(safeCollection, root.get(field).in(safeCollection));
        };
    }

    /**
     * Is empty specification.
     */
    protected <T> Specification<E> isEmpty(final String field) {
        return (root, cq, cb) -> cb.equal(root.get(field), "");
    }


    /**
     * Like specification.
     */
    protected Specification<E> startWith(final String field, final String title) {
        return (root, cq, cb) -> safe(title, cb.like(root.get(field), title + "%"));
    }

    /**
     * Save dto. Throws IllegalArgumentException if dto is null or already has an id.
     */
    public D save(final D dto) {
        checkArgument(nonNull(dto), "Param 'dto' is mandatory");
        checkArgument(isNull(dto.getId()), "Param 'dto' can not have an id before save first time");
        return mapper.map(getRepository().save(mapper.map(dto, entityClass())), dtoClass());
    }


    /**
     * Update dto. Throws IllegalArgumentException if dto is null or has no an id.
     */
    public void update(final D dto) {
        checkArgument(nonNull(dto), "Param 'dto' is mandatory");
        checkArgument(nonNull(dto.getId()), "Param 'dto' needs to have an id to update it");
        getRepository().save(mapper.map(dto, entityClass()));
    }


    /**
     * Update a list of dto.
     */
    public void update(final List<D> dtos) {
        safeStream(dtos).forEach(this::update);
    }


    /**
     * Save or update a list of dto.
     */
    public void saveOrUpdate(final List<D> dtos) {
        safeStream(dtos).forEach(this::saveOrUpdate);
    }


    /**
     * Save dto if it has an id or update it if it does not have it. Throws IllegalArgumentException if dto is null.
     */
    public void saveOrUpdate(final D dto) {
        checkArgument(nonNull(dto), "Param 'dto' is mandatory");
        getRepository().save(this.mapper.map(dto, entityClass()));
    }


    /**
     * Find by {id}. Throws IllegalArgumentException if {id} is null and NotFoundException if entity was not found.
     */
    public D findById(final Long id) {
        checkArgument(nonNull(id), "Param 'id' is mandatory");
        return getRepository().findById(id)
                .map(it -> mapper.map(it, dtoClass()))
                .orElseThrow(() -> new NotFoundException(format("Entity with id %s not found", id)));
    }


    /**
     * Get all.
     */
    public ImmutableList<D> findAll() {
        return ImmutableList.copyOf(mapper.map(getRepository().findAll(), dtoClass()));
    }

}
