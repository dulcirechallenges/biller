package com.insside.biller.persistence.store;

import com.insside.biller.persistence.repository.ServiceRepository;
import com.insside.biller.pojo.entity.Service;
import com.insside.biller.pojo.model.ServiceTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ServiceStore extends Store<Service, ServiceTO> {

    @Autowired
    private ServiceRepository repository;

    public Optional<ServiceTO> findByCode(final String code) {
        return repository.findByCode(code).map(d -> mapper.map(d, ServiceTO.class));
    }

    public void deleteById(final Long id) {
         repository.deleteById(id);
    }

    @Override
    protected JpaRepository<Service, Long> getRepository() {
        return repository;
    }

    @Override
    protected Class<Service> entityClass() {
        return Service.class;
    }

    @Override
    protected Class<ServiceTO> dtoClass() {
        return ServiceTO.class;
    }
}
