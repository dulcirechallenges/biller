package com.insside.biller.persistence.repository;

import com.insside.biller.pojo.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ServiceRepository extends JpaRepository<Service, Long> {

    Optional<Service> findByCode(final String code);

    Long deleteByCode(final String code);

}