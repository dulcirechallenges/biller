package com.insside.biller.persistence.repository;

import com.insside.biller.pojo.entity.Budget;
import com.insside.biller.pojo.entity.Service;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BudgetRepository extends JpaRepository<Budget, Long> {

    Optional<Budget> findByCode(final String code);

}
