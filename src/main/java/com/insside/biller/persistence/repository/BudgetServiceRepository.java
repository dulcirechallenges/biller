package com.insside.biller.persistence.repository;

import com.insside.biller.pojo.entity.BudgetService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BudgetServiceRepository extends JpaRepository<BudgetService, Long> {

    List<BudgetService> findByBudgetId(final Long id);

    public void deleteByBudgetId (final Long id);
}
