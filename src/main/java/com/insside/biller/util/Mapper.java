package com.insside.biller.util;

import com.google.common.collect.ImmutableMultimap;
import com.insside.biller.pojo.entity.Budget;
import com.insside.biller.pojo.entity.BudgetService;
import com.insside.biller.pojo.model.BudgetServiceTO;
import com.insside.biller.pojo.model.BudgetTO;
import com.insside.biller.pojo.model.ServiceTO;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Service
public class Mapper {

    private static final ImmutableMultimap<Class<?>, Class<?>> MAPPINGS = ImmutableMultimap.<Class<?>, Class<?>>builder()
            .put(com.insside.biller.pojo.entity.Service.class, ServiceTO.class)
            .put(Budget.class, BudgetTO.class)
            .put(BudgetService.class, BudgetServiceTO.class)
            .build();


    private static final ImmutableMultimap<Class<?>, String> EXCLUDED_FIELDS = ImmutableMultimap.<Class<?>, String>builder()
            .build();
    
    
    private MapperFacade mapperFacade;

    @PostConstruct
    public void init() {
        final MapperFactory factory = new DefaultMapperFactory.Builder().build();


        for (Map.Entry<Class<?>, Class<?>> entry : MAPPINGS.entries()) {
            final ClassMapBuilder<?, ?> builder = factory.classMap(entry.getKey(), entry.getValue());
            CollectionUtils.safeStream(EXCLUDED_FIELDS.get(entry.getValue())).forEach(builder::exclude);
            builder.byDefault().register();
        }
        mapperFacade = factory.getMapperFacade();
    }

    public <I, O> O map(final I input, final Class<O> clazz) {
        return mapperFacade.map(input, clazz);
    }

    public <I, O> List<O> map(final List<I> input, final Class<O> clazz) {
        return CollectionUtils.safeStream(input).map(it -> this.map(it, clazz)).collect(toList());
    }

    public <I, O> void map(final I input, final O output) {
        mapperFacade.map(input, output);
    }

    public static ImmutableMultimap<Class<?>, Class<?>> getMappings() {
        return MAPPINGS;
    }
    
    public static ImmutableMultimap<Class<?>, String> getExcludedFields() {
        return EXCLUDED_FIELDS;
    }
}