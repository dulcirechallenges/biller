package com.insside.biller.service;

import com.google.common.collect.ImmutableList;
import com.insside.biller.exception.InssideServiceException;
import com.insside.biller.persistence.store.ServiceStore;
import com.insside.biller.pojo.enums.ErrorStatus;
import com.insside.biller.pojo.model.ServiceTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ServiceService {

    @Autowired
    private ServiceStore serviceStore;

    public ServiceTO save(final ServiceTO serviceTO) {
        try {
            return serviceStore.save(serviceTO);
        } catch (Exception e) {
            log.warn(String.format("Error saving service with code:%s. Error:%s", serviceTO.getCode(), e.getMessage()));
            throw new InssideServiceException(String.format("Error saving service with code:%s", serviceTO.getCode()));
        }
    }

    public void update(final ServiceTO serviceTO) {
        try {
            serviceStore.update(serviceTO);
        } catch (Exception e) {
            log.warn(String.format("Error updating service with code:%s. Error:%s", serviceTO.getCode(), e.getMessage()));
            throw new InssideServiceException(String.format("Error updating service with code:%s", serviceTO.getCode()));
        }
    }

    public ServiceTO getServiceByCode(final String code) {
        try {
            return serviceStore.findByCode(code).get();
        } catch (Exception e) {
            throw new InssideServiceException(ErrorStatus.RESOURCE_NOT_FOUND.getDescription());
        }
    }

    public ImmutableList<ServiceTO> findAllServices() {
        return serviceStore.findAll();
    }

    public void delete(final ServiceTO serviceTO) {
        serviceStore.update(serviceTO);
    }

    public void delete(final Long id) {
        serviceStore.deleteById(id);
    }
}
