package com.insside.biller.service;

import com.google.common.collect.ImmutableList;
import com.insside.biller.exception.InssideBudgetException;
import com.insside.biller.persistence.store.BudgetServiceStore;
import com.insside.biller.persistence.store.BudgetStore;
import com.insside.biller.persistence.store.ServiceStore;
import com.insside.biller.pojo.entity.Budget;
import com.insside.biller.pojo.enums.ErrorStatus;
import com.insside.biller.pojo.model.BudgetServiceTO;
import com.insside.biller.pojo.model.BudgetTO;
import com.insside.biller.pojo.model.request.ServiceBudgetRequest;
import com.insside.biller.pojo.model.ServiceTO;
import com.insside.biller.pojo.model.response.BudgetResponseTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class BudgetService {

    @Autowired
    private BudgetStore budgetStore;

    @Autowired
    private ServiceStore serviceStore;


    @Autowired
    private BudgetServiceStore budgetServiceStore;

    public BudgetTO save(final BudgetTO budgetRequestTO) {
        try {
            final List<ServiceBudgetRequest> servicesTO = budgetRequestTO.getServiceTOList();
            BudgetTO budgetTO = budgetStore.save(budgetRequestTO);
            servicesTO.stream().forEach(service -> {
                BudgetServiceTO budgetServiceTO = BudgetServiceTO.builder().budget(budgetTO).service(service.getServiceTO()).serviceType(service.getServiceTypeCode()).build();
                budgetServiceTO.setStatus(true);
                budgetServiceStore.save(budgetServiceTO);
            } );
            return budgetTO;
        }catch (Exception e){
            log.warn(String.format("Error saving budget with code:%s. Error:%s",budgetRequestTO.getCode(),e.getMessage()));
            throw new InssideBudgetException(String.format("Error saving budget with code:%s. Error:%s",budgetRequestTO.getCode(),e.getMessage()));
        }
    }

    public void update(final BudgetTO budgetRequestTO){
        try {
            final  List<BudgetServiceTO> budgetServiceTOList = budgetServiceStore.findByBudgetId(budgetRequestTO.getId());
            budgetServiceTOList.stream().forEach(budgetService -> {
                budgetServiceStore.deleteById(budgetService.getId());
            });
            final List<ServiceBudgetRequest> servicesTO = budgetRequestTO.getServiceTOList();
            servicesTO.stream().forEach(service -> {
                BudgetServiceTO budgetServiceTO = BudgetServiceTO.builder().budget(budgetRequestTO).service(service.getServiceTO()).serviceType(service.getServiceTypeCode()).build();
                budgetServiceTO.setStatus(true);
                budgetServiceStore.save(budgetServiceTO);
            } );
        }catch (Exception e){
            log.warn(String.format("Error updating service with code:%s. Error:%s",budgetRequestTO.getCode(),e.getMessage()));
            throw new InssideBudgetException(String.format("Error saving budget with code:%s. Error:%s",budgetRequestTO.getCode(),e.getMessage()));
        }
    }

    public BudgetResponseTO getBudgetByCode(final String code) {
        try {
            final BudgetTO budgetTO = budgetStore.findByCode(code).get();
            final List<BudgetServiceTO> budgetServiceTO = budgetServiceStore.findByBudgetId(budgetTO.getId());
            return BudgetResponseTO.builder().budgetTO(budgetTO).serviceTOList(budgetServiceTO).build();
        }catch (Exception e){
            throw  new InssideBudgetException(ErrorStatus.RESOURCE_NOT_FOUND.getDescription());
        }
    }

    public ImmutableList<BudgetTO> findAllBudgets(){
        return budgetStore.findAll();
    }

    public void delete(final Long id){
        budgetStore.deleteById(id);
    }
}
